<?php

//Boolean example started form here

$decision=True;
if($decision) echo "The decision is True<br>";

$decision=False;
if($decision) echo "The decision is False<br>";

//Boolean example  ended here


//Interger example started form here

$value = 100;
echo $value . "<br>";

//Interger example  ended here

//Float/Double example started form here

$value = 35.86;
echo $value . "<br>";

//Float/Double example  ended here

//String example start here

    $var = 100;

    $string1 = 'This is double quoted string $var<br>';

    $string2 = "This is double quoted string $var<br>";

    echo $string1 . $string2;



$heredocString=<<<BITM

    this is herdoc example line1 $var <br>
    this is herdoc example line2 $var <br>

BITM;

$nowdocString=<<<'BITM'
    this is herdoc example line1 $var <br>
    this is herdoc example line2 $var <br>
BITM;

   echo $heredocString . "<br> <br>" . $nowdocString;

//String example end here


$arr = array(1,2,3,4,5,6,7,8,9);
print_r($arr);

echo "<br>";

$arr = array("BMW", "TOYATA", "NISSAN", "FERRARI" );
    print_r($arr);

echo "<br>";

$ageArray = array("Arif"=>30,"Karim"=>40,"Rahim"=>25);
print_r($ageArray);


echo "<br>";

echo "The age of Karim is " . $ageArray["Karim"];

echo "<br>";
